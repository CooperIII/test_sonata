<?php

namespace App\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\Form\Extension\Core\Type\TextType;

final class CategoryAdmin extends AbstractAdmin
{
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper->add('name', TextType::class);
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper->add('name');
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper->addIdentifier('name');
    }

    public function createQuery($context = 'list')
    {
        $query = parent::createQuery($context);
        //$query = new ProxyQuery($qb);
        $alias = $query->getRootAlias();
        //$query->addSelect($alias);
        $query->leftJoin($query->getRootAlias().'.blogPosts', 'b');
        //$query->addSelect(" PARTIAL  SUM(b.sum) AS savings_value");
        //$query->having("SUM(b.sum) > 100");
        $query->groupBy("{$alias}.id");
       // $query->addSelect(' '. $query->getRootAlias() . '.sum as total');
        //$query->groupBy($query->getRootAlias().'.id');
dump(get_class($query));
        return $query;
    }
}